package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Customer;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Util.Factory.FactoryCustomer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FactoryCustomerTest {
    private final FactoryCustomer factoryCustomer = new FactoryCustomer();

    @Test
    public void makeNewCustomer() throws ModelsException {
        String[] customerData = new String[]{ "002", "1234567891234", "Fulano", "Rural" };
        Customer customer = factoryCustomer.makeDTO( customerData );

        assertEquals( "1234567891234", customer.getCnpj() );
        assertEquals( "Fulano", customer.getName() );
        assertEquals( "Rural", customer.getBusinessArea() );
    }
}
