package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.SellerNonexistentException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.CPFInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.NameInvalidExpcetion;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.SalaryNegativeException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatorSellerDataTest {
    @Test
    public void allValidDataReturnTrue() throws ModelsException {
        String[] sellerData = new String[]{ "001", "12345678901", "Fulano", "4000.99" };

        ValidatorSellerData.validateData( sellerData );
    }

    @Test
    public void invalidCPFLengthThrowCPFInvalidException() {
        String[] sellerData = new String[]{ "001", "1234", "Fulano", "4000.99" };

        assertThrows( CPFInvalidException.class, () -> {
            ValidatorSellerData.validateData( sellerData );
        });
    }

    @Test
    public void invalidCPFCharThrowCPFInvalidException() {
        String[] sellerData = new String[]{ "001", "123dd678901", "Fulano", "4000.99" };

        assertThrows( CPFInvalidException.class, () -> {
            ValidatorSellerData.validateData( sellerData );
        });
    }

    @Test
    public void invalidNameLengthReturnFalse() {
        String[] sellerData = new String[]{ "001", "12345678901", "F", "4000.99" };

        assertThrows( NameInvalidExpcetion.class, () -> {
            ValidatorSellerData.validateData( sellerData );
        });
    }

    @Test
    public void invalidNumberSeparatorInSalaryThrowsSalaryException() {
        String[] sellerData = new String[]{ "001", "12345678901", "Fulano", "4000,99" };

        assertThrows( SalaryNegativeException.class, () -> {
            ValidatorSellerData.validateData( sellerData );
        });
    }
}
