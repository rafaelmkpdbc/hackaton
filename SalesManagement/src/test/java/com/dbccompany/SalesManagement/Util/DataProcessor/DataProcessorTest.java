package com.dbccompany.SalesManagement.Util.DataProcessor;

import com.dbccompany.SalesManagement.DTO.Customer;
import com.dbccompany.SalesManagement.DTO.Item;
import com.dbccompany.SalesManagement.DTO.Sale;
import com.dbccompany.SalesManagement.DTO.Seller;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DataProcessorTest {

    @Test
    public void insert3SellersGet3Sellers() {
        ArrayList<Serializable> input = new ArrayList<Serializable>();
        input.add( new Seller( "25835243057", "Pedro", 50000 ) );
        input.add( new Seller( "13704443034", "Paulo", (float) 40000.99) );
        input.add( new Seller( "78360209081", "João", (float) 49000.99) );
        input.add( new Customer( "44932373000176", "José Da Silva", "Rural" ) );
        input.add( new Customer( "85299223000154", "Eduardo Pereira", "Rural" ) );

        ArrayList<Item> itemsSale1 = new ArrayList<Item>();
        itemsSale1.add( new Item("1", 10, 100 ) );
        itemsSale1.add( new Item("2", 30, (float) 2.5) );
        itemsSale1.add( new Item("3", 40, (float) 3.1) );
        input.add( new Sale( "10", "Pedro" ,itemsSale1 ) );

        ArrayList<Item> itemsSale2 = new ArrayList<Item>();
        itemsSale2.add( new Item("1", 34, 10 ) );
        itemsSale2.add( new Item("2", 33, (float) 1.5) );
        itemsSale2.add( new Item("3", 40, (float) 0.1) );
        input.add( new Sale( "08", "Paulo" ,itemsSale2 ) );

        ArrayList<Item> itemsSale3 = new ArrayList<Item>();
        itemsSale3.add( new Item("1", 40, 100 ) );
        itemsSale3.add( new Item("2", 10, (float) 2) );
        itemsSale3.add( new Item("3", 1, (float) 3.1) );
        itemsSale3.add( new Item("4", 5, (float) 25.59) );
        input.add( new Sale( "01", "João" ,itemsSale3 ) );

        ArrayList<String> output = DataProcessor.processData( input );

        assertEquals( "2" , output.get(0).substring( (output.get(0).length() - 1) , output.get(0).length()) );
        assertEquals( "3" , output.get(1).substring( (output.get(1).length() - 1) , output.get(1).length()) );
        assertEquals( "01" , output.get(2).substring( (output.get(2).length() - 2) , output.get(2).length()) );
        assertTrue( output.get(3).contains("Paulo") );
    }

    @Test
    public void validSalesReturnValidOutput() {
        ArrayList<Serializable> input = new ArrayList<Serializable>();
        input.add(new Seller("25835243057", "Pedro", 50000));
        input.add(new Seller("13704443034", "Paulo", (float) 40000.99));
        input.add(new Customer("44932373000176", "José Da Silva", "Rural"));
        input.add(new Customer("85299223000154", "Eduardo Pereira", "Rural"));

        ArrayList<Item> itemsSale1 = new ArrayList<Item>();
        itemsSale1.add(new Item("1", 1, 1000));
        input.add(new Sale("10", "Pedro", itemsSale1));

        ArrayList<Item> itemsSale2 = new ArrayList<Item>();
        itemsSale2.add(new Item("2", 10, (float) 0.50));
        input.add(new Sale("08", "Paulo", itemsSale2));

        ArrayList<String> output = DataProcessor.processData(input);

        assertEquals( "2" , output.get(0).substring( (output.get(0).length() - 1) , output.get(0).length()) );
        assertEquals( "2" , output.get(1).substring( (output.get(1).length() - 1) , output.get(1).length()) );
        assertEquals( "10" , output.get(2).substring( (output.get(2).length() - 2) , output.get(2).length()) );
        assertTrue( output.get(3).contains("Paulo") );
    }

    @Test
    public void validSalesReturnValidOutput2(){
        ArrayList<Serializable> input = new ArrayList<Serializable>();
        input.add( new Seller( "25835243057", "Pedro", 50000 ) );
        input.add( new Seller( "13704443034", "Paulo", (float) 40000.99) );
        input.add( new Customer( "44932373000176", "José Da Silva", "Rural" ) );
        input.add( new Customer( "85299223000154", "Eduardo Pereira", "Rural" ) );

        ArrayList<Item> itemsSale1 = new ArrayList<Item>();
        itemsSale1.add( new Item("01", 10, 300 ) );
        itemsSale1.add( new Item("02", 30, (float) 2.5) );
        itemsSale1.add( new Item("03", 40, (float) 3.1) );
        input.add( new Sale( "10", "Pedro" ,itemsSale1 ) );

        ArrayList<Item> itemsSale2 = new ArrayList<Item>();
        itemsSale2.add( new Item("01", 5, 1 ) );
        itemsSale2.add( new Item("02", 6, (float) 1.5) );
        itemsSale2.add( new Item("03", 7, (float) 0.1) );
        input.add( new Sale( "08", "Paulo" ,itemsSale2 ) );

        ArrayList<Item> itemsSale3 = new ArrayList<Item>();
        itemsSale3.add( new Item("1", 1, 5 ) );
        itemsSale3.add( new Item("2", 6, (float) 2) );
        itemsSale3.add( new Item("3", 1, (float) 3.1) );
        itemsSale3.add( new Item("4", 5, (float) 25.59) );
        input.add( new Sale( "01", "João" ,itemsSale3 ) );

        ArrayList<String> output = DataProcessor.processData(input);

        assertEquals( "2" , output.get(0).substring( (output.get(0).length() - 1) , output.get(0).length()) );
        assertEquals( "2" , output.get(1).substring( (output.get(1).length() - 1) , output.get(1).length()) );
        assertEquals( "10" , output.get(2).substring( (output.get(2).length() - 2) , output.get(2).length()) );
        assertTrue( output.get(3).contains("Paulo") );
    }


    @Test
    public void validSalesReturnValidOutput3(){
        ArrayList<Serializable> input = new ArrayList<Serializable>();
        input.add( new Seller( "25835243057", "Pedro", 50000 ) );
        input.add( new Seller( "13704443034", "Paulo", (float) 40000.99) );
        input.add( new Customer( "44932373000176", "José Da Silva", "Rural" ) );
        input.add( new Customer( "85299223000154", "Eduardo Pereira", "Rural" ) );

        ArrayList<Item> itemsSale1 = new ArrayList<Item>();
        itemsSale1.add( new Item("01", 10, 100 ) );
        itemsSale1.add( new Item("02", 30, (float) 2.5) );
        input.add( new Sale( "10", "Pedro" ,itemsSale1 ) );

        ArrayList<String> output = DataProcessor.processData(input);

        assertEquals( "2" , output.get(0).substring( (output.get(0).length() - 1) , output.get(0).length()) );
        assertEquals( "2" , output.get(1).substring( (output.get(1).length() - 1) , output.get(1).length()) );
        assertEquals( "10" , output.get(2).substring( (output.get(2).length() - 2) , output.get(2).length()) );
        assertTrue( output.get(3).contains("Pedro") );
    }

}
