package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.CNPJInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.SellerNonexistentException;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatorSaleDataTest {
    @Test
    public void allValidDataReturnTrue() throws ModelsException {
        String type = "003";
        String id = "123";
        String seller = "Pedro";
        String items = "[1-10-1.5,2-10-2.5,3-20-3.10]";
        String[] rawData = new String[]{ type, id, items, seller };

        ValidatorSaleData.validateData( rawData );
    }

    @Test
    public void invalidSellerThrowsException() {
        String type = "003";
        String id = "123";
        String seller = "P";
        String items = "[1-10-1.5,2-10-2.5,3-20-3.10]";
        String[] rawData = new String[]{ type, id, items, seller };

        assertThrows( SellerNonexistentException.class, () -> {
            ValidatorSaleData.validateData( rawData );
        });
    }
}
