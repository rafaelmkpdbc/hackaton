package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Seller;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Util.Factory.FactorySeller;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactorySellerTest {

    private FactorySeller factorySeller = new FactorySeller();

    @Test
    public void makeNewSeller() throws ModelsException {
        String[] customerData = new String[]{ "001", "123456789", "Fulano", "4000.99" };
        Seller seller = factorySeller.makeDTO( customerData );

        assertEquals( "123456789", seller.getCpf() );
        assertEquals( "Fulano", seller.getName() );
        assertEquals( "4000.99", String.valueOf( seller.getSalary() ) );
    }
}
