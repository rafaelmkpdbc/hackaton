package com.dbccompany.SalesManagement.Util;

import com.dbccompany.SalesManagement.DTO.Customer;
import com.dbccompany.SalesManagement.DTO.Sale;
import com.dbccompany.SalesManagement.DTO.Seller;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.ArrayList;

import static  org.junit.jupiter.api.Assertions.*;

public class RawParserTest {
    @Test
    public void checkForCorrectClasses() throws Exception {
        String row1 = "001ç1234567891234çPedroç50000";
        String row2 = "001ç3245678865434çPauloç40000.99";
        String row3 = "002ç2345675434544345çJose da SilvaçRural";
        String row4 = "002ç2345675433444345çEduard PereiraçRural";
        String row5 = "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro";
        String row6 = "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo";

        String[] rawData = new String[]{ row1, row2, row3, row4, row5, row6};

        ArrayList<Serializable> objects = RawParser.classifyData( rawData );

        assertTrue( objects.get( 0 ) instanceof Seller );
        assertTrue( objects.get( 1 ) instanceof Seller );
        assertTrue( objects.get( 2 ) instanceof Customer );
        assertTrue( objects.get( 3 ) instanceof Customer );
        assertTrue( objects.get( 4 ) instanceof Sale );
        assertTrue( objects.get( 5 ) instanceof Sale);
    }

    @Test
    public void checkForCorrectSeller() throws Exception{
        String row1 = "001ç1234567891234çPedroç50000";
        String row2 = "001ç3245678865434çPauloç40000.99";
        String row3 = "002ç2345675434544345çJose da SilvaçRural";
        String row4 = "002ç2345675433444345çEduard PereiraçRural";
        String row5 = "003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro";
        String row6 = "003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo";

        String[] rawData = new String[]{ row1, row2, row3, row4, row5, row6};

        ArrayList<Serializable> objects = RawParser.classifyData( rawData );

        Seller seller = ( Seller ) objects.get( 0 );

        assertEquals( "1234567891234", seller.getCpf() );
        assertEquals( "Pedro", seller.getName() );
        assertEquals( 50000, seller.getSalary() );
    }
}
