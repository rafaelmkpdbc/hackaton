package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Sale;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.QuantityZeroInItemException;
import com.dbccompany.SalesManagement.Util.Factory.FactorySale;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FactorySaleTest {
    private FactorySale factorySale = new FactorySale();

    @Test
    public void makeNewSale() throws ModelsException {
        String type = "003";
        String id = "123";
        String seller = "Pedro";
        String items = "[1-10-1.5,2-10-2.5,3-20-3.10]";
        String[] rawData = new String[]{ type, id, items, seller };

        Sale sale = factorySale.makeDTO( rawData );

        assertEquals( "123", sale.getId() );
        assertEquals( "Pedro", sale.getSeller() );
        assertTrue( sale.getItems().size() > 0 );
    }
}
