package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.CNPJInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.NameInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatorCustomerDataTest {
    @Test
    public void allValidDataReturnTrue() throws Exception {
        String[] customerData = new String[]{ "002", "12345678901234", "Fulano", "Rural" };

        ValidatorCustomerData.validateData( customerData );
    }

    @Test
    public void invalidCNPJLengthThrowsInvalidCNPJException() {
        String[] customerData = new String[]{ "002", "1234", "Fulano", "Rural" };

        assertThrows( CNPJInvalidException.class, () -> {
            ValidatorCustomerData.validateData( customerData );
        });
    }

    @Test
    public void invalidCNPJCharThrowsCNPJException() {
        String[] customerData = new String[]{ "002", "1234567a91234", "Fulano", "Rural" };

        assertThrows( CNPJInvalidException.class, () -> {
            ValidatorCustomerData.validateData( customerData );
        });
    }

    @Test
    public void invalidNameLengthThrowsInvalidNameException() {
        String[] customerData = new String[]{ "002", "12345678901234", "F", "Rural" };

        assertThrows( NameInvalidException.class, () -> {
            ValidatorCustomerData.validateData( customerData );
        });
    }

    @Test
    public void invalidBusinessAreaLengthThrowsInvalidNameException() {
        String[] customerData = new String[]{ "002", "12345678901234", "Fulano", "R" };

        assertThrows( NameInvalidException.class, () -> {
            ValidatorCustomerData.validateData( customerData );
        });
    }
}
