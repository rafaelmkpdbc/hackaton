package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.NameInvalidExpcetion;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.CPFInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.SalaryNegativeException;

import java.util.*;

public abstract class ValidatorSellerData extends ValidatorDataGeneric {
    private static final HashSet<Character> VALID_CPF_CHARACTERS = new HashSet<>(
            Arrays.asList( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
    );

    public static void validateData( String[] dataSet ) throws ModelsException {
        validateCPF( dataSet[1] );
        validateName( dataSet[2] );
        validateSalary( dataSet[3] );
    }

    public static void validateCPF( String cpf ) throws CPFInvalidException {
        if( !hasValidLength( cpf ) || !isAllValidCharacters( cpf ) ) {
            throw new CPFInvalidException();
        }
    }

    private static void validateName( String name ) throws NameInvalidExpcetion {
        if ( name.length() <= 1 ) {
            throw new NameInvalidExpcetion();
        }
    }


    private static void validateSalary( String salary ) throws SalaryNegativeException {
        if ( salary.contains( "," ) ||
             salary.charAt( 0 ) == '.' ||
             salary.charAt( salary.length() - 1 ) == '.' ||
             Float.parseFloat( salary ) < 0
        ) {
            throw new SalaryNegativeException();
        }
    }

    private static boolean isAllValidCharacters( String cpf ) {
        for( char number : cpf.toCharArray() )  {
            if( !VALID_CPF_CHARACTERS.contains( number ) ) {
                return false;
            }
        }
        return true;
    }

    private static boolean hasValidLength( String cpf ) {
        return cpf.length() >= 11;
    }
}