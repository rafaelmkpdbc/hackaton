package com.dbccompany.SalesManagement.Util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DatUtils {
    public final static Path DIRECTORY_PATH_IN = Paths.get(System.getProperty("user.home") + "/data/in");
    public final static Path DIRECTORY_PATH_OUT = Paths.get(System.getProperty("user.home") + "/data/out");

    public static List<String> readAllLines(String fileName) {
        List<String> lines = new ArrayList<>();

        try {
            lines = Files.readAllLines(Paths.get(DIRECTORY_PATH_IN + "/" + fileName));
        } catch (IOException e) {
            Log.registraErro(e.getMessage());
        }

        return lines;
    }

    public static void writeFile(String fileName, List<String> lines) {
        byte[] linesBytes = organizeContent(lines).getBytes(StandardCharsets.UTF_8);

        try {
            Files.write(Paths.get(DIRECTORY_PATH_OUT + "/" + filterFilename(fileName) + ".done.dat"), linesBytes);
        } catch (IOException e) {
            Log.registraErro(e.getMessage());
        }
    }

    public static String filterFilename(String context) {
        String fileName = context.split("\\.")[0];
        return fileName;
    }

    public static Boolean filterExtension(String context) {
        String extension = context.split("\\.")[1];
        return extension.equals("dat");
    }

    public static void checkDirectoryIn() {
        if (!Files.exists(DIRECTORY_PATH_IN)) {
            try {
                Files.createDirectories(DIRECTORY_PATH_IN);
            } catch (IOException e) {
                Log.registraErro(e.getMessage());
            }
        }
    }

    public static void checkDirectoryOn() {
        if (!Files.exists(DIRECTORY_PATH_OUT)) {
            try {
                Files.createDirectories(DIRECTORY_PATH_OUT);
            } catch (IOException e) {
                Log.registraErro(e.getMessage());
            }
        }
    }

    private static String organizeContent(List<String> lines) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            stringBuilder.append(line);

            if (i != lines.size() - 1) {
                stringBuilder.append("\r");
            }
        }

        return stringBuilder.toString();
    }
}
