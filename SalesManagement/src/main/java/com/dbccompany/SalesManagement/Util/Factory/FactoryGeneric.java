package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

import java.io.Serializable;

public abstract class FactoryGeneric {
    public abstract Serializable makeDTO( String[] data ) throws ModelsException;
}
