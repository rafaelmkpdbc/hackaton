package com.dbccompany.SalesManagement.Util;

import com.dbccompany.SalesManagement.SalesManagementApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {
    private static Logger logger = LoggerFactory.getLogger(SalesManagementApplication.class);

    public static void registraErro(){
        logger.error( "An error has ocured" );
    }

    public static void registraErro( String message ){
        logger.error( message );
    }


}
