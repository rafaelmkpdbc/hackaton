package com.dbccompany.SalesManagement.Util.DataProcessor;

import com.dbccompany.SalesManagement.DTO.Customer;
import com.dbccompany.SalesManagement.DTO.Item;
import com.dbccompany.SalesManagement.DTO.Sale;
import com.dbccompany.SalesManagement.DTO.Seller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataProcessor {

    public static ArrayList<String> processData(ArrayList<Serializable> parsedData ) {
        int quantitySeller = 0;
        int quantityCustomer = 0;
        String idHighestSale = "";
        float highestSaleValue = 0;
        HashMap<String, Float> sellers = new HashMap<String, Float>();

        for( Serializable data : parsedData ) {
            if( data instanceof Seller) {
                quantitySeller++;
            }else if( data instanceof Customer ) {
                quantityCustomer++;
            }else if( data instanceof Sale) {
                float saleValue = saleValue( ((Sale) data).getItems() );
                if( saleValue > highestSaleValue ) {
                    idHighestSale = ((Sale) data).getId();
                    highestSaleValue = saleValue;
                }
                String sellerName = ((Sale) data).getSeller();
                try{
                    sellers.put( sellerName, sellers.get( sellerName ) + saleValue );
                }catch( Exception e ) {
                    sellers.put( sellerName, saleValue );
                }
            }
        }
        Map.Entry<String, Float> min = null;
        for(Map.Entry<String, Float> element : sellers.entrySet() ) {
            if (min == null || ( min.getValue() > element.getValue() ) ) {
                min = element;
            }
        }

        ArrayList<String> processedData = new ArrayList<String>(4);
        processedData.add( "Quantidade de Clientes: " + quantityCustomer);
        processedData.add( "Quantidade de Vendedores: " + quantitySeller);
        processedData.add( "ID Venda mais alta: " + idHighestSale );
        processedData.add( "Pior Vendedor: " + min.getKey() );

        return processedData;
    }

    private static float saleValue ( ArrayList<Item> items ) {
        float value = 0;
        for( Item item : items ){
            value += item.getPrice() * item.getQuantity();
        }
        return value;
    }


}
