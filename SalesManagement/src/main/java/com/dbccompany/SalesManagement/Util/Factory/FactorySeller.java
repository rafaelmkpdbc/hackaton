package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Seller;
import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.MissingDataCustomerException;
import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.NameInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.InvalidSeparatorCharException;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.PriceNegativeInItemException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.CPFInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.MissingDataSellerExpcetion;
import com.dbccompany.SalesManagement.Exception.ModelsException.SellerException.SalaryNegativeException;
import com.dbccompany.SalesManagement.Exception.ModelsException.TooManyDataException;

public class FactorySeller extends FactoryGeneric {
    public Seller makeDTO( String[] data ) throws ModelsException {

        if(data.length != 4){
            throw data.length > 4 ? new TooManyDataException() : new MissingDataSellerExpcetion();
        }

        if(data[1].contains("A")){ // VERIFICAR O CPF - NUMERO DE CARACTERES SE EXISTE LETRA NO CPF
            throw new CPFInvalidException();
        }

        if(data[2].length() == 1){ // VERIFICAR UM CARACTER NO NOME
            throw new NameInvalidException();
        }

        if(Double.parseDouble(data[3]) < 0) { // VERIFICAR PREÇO NEGATIVO E SE POSSUI VIRGULA
            throw data[3].contains(",") ? new InvalidSeparatorCharException() : new SalaryNegativeException();
        }

        return new Seller( data[1], data[2], Float.parseFloat( data[3] ) );
    }
}
