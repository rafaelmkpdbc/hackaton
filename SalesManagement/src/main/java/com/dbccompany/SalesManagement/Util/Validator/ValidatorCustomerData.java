package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.CNPJInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException.NameInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

import java.util.Arrays;
import java.util.HashSet;

public abstract class ValidatorCustomerData extends ValidatorDataGeneric {
    private static final HashSet<Character> VALID_CNPJ_CHARACTERS = new HashSet<>(
            Arrays.asList( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
    );

    public static void validateData( String[] dataSet ) throws ModelsException {
        validateCNPJ( dataSet[1] );
        validateName( dataSet[2] );
        validateBusinessArea( dataSet[3] );
    }

    private static void validateName( String name ) throws NameInvalidException {
        if ( name.length() <= 1 ) {
            throw new NameInvalidException();
        }
    }

    private static void validateBusinessArea( String businessArea ) throws NameInvalidException {
        if( businessArea.length() <= 1 ) {
            throw new NameInvalidException();
        }
    }

    private static void validateCNPJ( String cnpj ) throws CNPJInvalidException {
        if( !CNPJHasValidLength( cnpj ) || !CNPJHasAllValidCharacters( cnpj ) ) {
            throw new CNPJInvalidException();
        }
    }

    private static boolean CNPJHasAllValidCharacters( String cnpj ) {
        for( char number : cnpj.toCharArray() )  {
            if( !VALID_CNPJ_CHARACTERS.contains( number ) ) {
                return false;
            }
        }
        return true;
    }

    private static boolean CNPJHasValidLength( String cnpj ) {
        return cnpj.length() >= 14;
    }
}
