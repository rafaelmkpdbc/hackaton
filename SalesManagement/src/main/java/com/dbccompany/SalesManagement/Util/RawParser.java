package com.dbccompany.SalesManagement.Util;

import com.dbccompany.SalesManagement.Exception.InputsException.*;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Util.Factory.*;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class RawParser {
    private static final HashMap<Integer, FactoryGeneric> IDENTIFIER_MAP = new HashMap<Integer, FactoryGeneric>(){{
        put( 1, new FactorySeller() );
        put( 2, new FactoryCustomer() );
        put( 3, new FactorySale() );
    }};

    private static final String DATA_SEPARATOR = "ç";

    public static ArrayList<Serializable> classifyData( String[] rawInputLines ) throws ModelsException, InputException {
        return buildObjects( rawInputLines );
    }

    private static ArrayList<Serializable> buildObjects( String[] rows ) throws ModelsException, InputException {
        ArrayList<Serializable> objects = new ArrayList<>();

        for( String row : rows ) {
            String[] dataSet = row.split( DATA_SEPARATOR );
            Serializable dto = buildSingleObject( dataSet );
            objects.add( dto );
        }
        return objects;
    }

    private static Serializable buildSingleObject( String[] parameters ) throws InputException, ModelsException {
        Integer type = Integer.parseInt( parameters[0] );

        if(type > 3 || type < 0){ // VERIFICAR O TIPO (CLIENTE, VENDEDOR, VENDA)
           throw new IDTypeInvalidException();
        }

        return IDENTIFIER_MAP.get( type ).makeDTO( parameters );
    }
}
