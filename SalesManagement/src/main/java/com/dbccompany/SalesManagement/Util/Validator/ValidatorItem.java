package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.ItemQttInvalidException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.PriceNegativeInItemException;

import java.util.Arrays;
import java.util.HashSet;

public abstract class ValidatorItem {
    private static final HashSet<Character> VALID_DATA_CHARACTERS = new HashSet<>(
        Arrays.asList( '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.' )
    );

    public static void validateData( String[] item ) throws ModelsException {
        hasAllValidCharacters( item );
        validateQuantity( item[1] );
        validatePrice( item[2] );
    }

    private static void hasAllValidCharacters( String[] data ) throws ModelsException {
        for( String dataValue : data ) {
            for( char symbol : dataValue.toCharArray() ) {
                if( !VALID_DATA_CHARACTERS.contains( symbol ) ) {
                    throw new ModelsException( "Caractere invalido em atributo de item" );
                }
            }
        }
   }

    private static void validateQuantity( String quantity ) throws ItemQttInvalidException {
        if( Integer.parseInt( quantity ) < 0 ){
            throw new ItemQttInvalidException();
        }
    }

    private static void validatePrice( String price ) throws PriceNegativeInItemException {
        if( Float.parseFloat( price ) < 0 ) {
            throw new PriceNegativeInItemException();
        }
    }
}
