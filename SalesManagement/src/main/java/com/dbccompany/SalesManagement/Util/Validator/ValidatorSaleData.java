package com.dbccompany.SalesManagement.Util.Validator;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.SellerNonexistentException;

public class ValidatorSaleData extends ValidatorDataGeneric{
    public static void validateData( String[] dataSet ) throws ModelsException {
        validateSeller( dataSet[3] );
    }

    private static void validateSeller( String seller ) throws SellerNonexistentException {
        if( seller.length() <= 1 ) {
            throw new SellerNonexistentException();
        }
    }
}
