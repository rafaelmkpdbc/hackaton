package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Item;
import com.dbccompany.SalesManagement.DTO.Sale;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.MissingDataSaleExpcetion;
import com.dbccompany.SalesManagement.Exception.ModelsException.TooManyDataException;
import com.dbccompany.SalesManagement.Util.Validator.ValidatorItem;
import com.dbccompany.SalesManagement.Util.Validator.ValidatorSaleData;

import java.util.ArrayList;

public class FactorySale extends FactoryGeneric {
    public Sale makeDTO( String[] data ) throws ModelsException {
        validateAmountData( data );
        ValidatorSaleData.validateData( data );

        String cleanedItemString =
                data[2].replace( "[", "" )
                       .replace( "]", "" );
        String[] itemStringSet = cleanedItemString.split( "," );
        ArrayList<Item> items = makeManyItems( itemStringSet );

        return new Sale( data[1], data[3], items );
    }

    private void validateAmountData( String[] data ) throws ModelsException {
        if(data.length != 4){
            throw data.length > 4 ? new TooManyDataException() : new MissingDataSaleExpcetion();
        }
    }

    private ArrayList<Item> makeManyItems( String[] stringSet ) throws ModelsException {
        ArrayList<Item> items = new ArrayList<>();
        for( String item : stringSet ) {
            String[] rawData = item.split( "-" );
            items.add( makeItem( rawData ) );
        }

        return items;
    }
    private Item makeItem( String[] itemData ) throws ModelsException {
        ValidatorItem.validateData( itemData );
        return new Item( itemData[0], Integer.parseInt( itemData[1] ), Float.parseFloat( itemData[2] ) );
    }
}
