package com.dbccompany.SalesManagement.Util.Factory;

import com.dbccompany.SalesManagement.DTO.Customer;
import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;
import com.dbccompany.SalesManagement.Exception.ModelsException.SaleException.MissingDataSaleExpcetion;
import com.dbccompany.SalesManagement.Exception.ModelsException.TooManyDataException;
import com.dbccompany.SalesManagement.Util.Validator.ValidatorCustomerData;

public class FactoryCustomer extends FactoryGeneric {
    public Customer makeDTO( String[] data ) throws ModelsException {
        validateAmountData( data );
        ValidatorCustomerData.validateData( data );
        return new Customer( data[1], data[2], data[3] );
    }

    private void validateAmountData( String[] data ) throws ModelsException {
        if(data.length != 4){
            throw data.length > 4 ? new TooManyDataException() : new MissingDataSaleExpcetion();
        }
    }
}
