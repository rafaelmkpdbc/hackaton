package com.dbccompany.SalesManagement.Batch.Tasklet;

import com.dbccompany.SalesManagement.Util.RawParser;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.Serializable;
import java.util.List;

public class LinesProcessor implements Tasklet, StepExecutionListener {

    private List<String> lines;
    private List<Serializable> serializableData;

    @Override
    public void beforeStep( StepExecution stepExecution ) {
        ExecutionContext executionContext = stepExecution
            .getJobExecution()
            .getExecutionContext();
        this.lines = ( List<String> ) executionContext.get( "lines" );
    }

    @Override
    public RepeatStatus execute( StepContribution contribution, ChunkContext chunkContext ) throws Exception {
        this.serializableData = RawParser.classifyData(this.lines.toArray(new String[this.lines.size()]));

        return RepeatStatus.FINISHED;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        stepExecution.getJobExecution().getExecutionContext().put("serializableData", this.serializableData);
        return ExitStatus.COMPLETED;
    }
}
