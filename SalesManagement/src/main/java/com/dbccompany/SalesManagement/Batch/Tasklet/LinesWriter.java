package com.dbccompany.SalesManagement.Batch.Tasklet;

import com.dbccompany.SalesManagement.Util.DatUtils;
import com.dbccompany.SalesManagement.Util.DataProcessor.DataProcessor;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LinesWriter implements Tasklet, StepExecutionListener {

    private List<String> lines;
    private String fileName;

    @Override
    public void beforeStep( StepExecution stepExecution ) {
        ExecutionContext executionContext = stepExecution
                .getJobExecution()
                .getExecutionContext();

        ArrayList<Serializable> serializableDataset = (ArrayList<Serializable>) executionContext.get( "serializableData" );

        this.lines = DataProcessor.processData(serializableDataset);
        this.fileName = executionContext.getString( "fileName" );
    }

    @Override
    public RepeatStatus execute( StepContribution contribution, ChunkContext chunkContext ) throws Exception {
        try {
            Files.createFile(Paths.get(DatUtils.DIRECTORY_PATH_OUT + "/" + fileName + ".done.dat"));
            DatUtils.writeFile(this.fileName, this.lines);
        } catch (IOException e) {
            e.printStackTrace();
            contribution.setExitStatus(ExitStatus.FAILED);
        }
        return RepeatStatus.FINISHED;
    }

    @Override
    public ExitStatus afterStep( StepExecution stepExecution ) {
        return ExitStatus.COMPLETED;
    }
}
