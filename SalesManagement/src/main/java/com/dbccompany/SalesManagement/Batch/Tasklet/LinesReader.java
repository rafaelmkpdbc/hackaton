package com.dbccompany.SalesManagement.Batch.Tasklet;

import com.dbccompany.SalesManagement.Util.DatUtils;
import org.springframework.batch.core.*;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.List;

public class LinesReader implements Tasklet, StepExecutionListener {

    private String fileName;
    private List<String> lines;

    @Override
    public void beforeStep( StepExecution stepExecution ) {
        JobParameters parameters = stepExecution.getJobParameters();
        this.fileName = parameters.getString( "filename" );
    }

    @Override
    public RepeatStatus execute( StepContribution contribution, ChunkContext chunkContext ) throws Exception {
        this.lines = DatUtils.readAllLines( fileName );

        return RepeatStatus.FINISHED;
    }

    @Override
    public ExitStatus afterStep( StepExecution stepExecution ) {
        stepExecution.getJobExecution().getExecutionContext().put( "lines", this.lines );
        stepExecution.getJobExecution().getExecutionContext().put( "fileName", DatUtils.filterFilename(this.fileName) );

        return ExitStatus.COMPLETED;
    }
}
