package com.dbccompany.SalesManagement.Batch;

import com.dbccompany.SalesManagement.Batch.Tasklet.LinesProcessor;
import com.dbccompany.SalesManagement.Batch.Tasklet.LinesReader;
import com.dbccompany.SalesManagement.Batch.Tasklet.LinesWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchTaskletsConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public LinesReader linesReader() {
        return new LinesReader();
    }

    @Bean
    public LinesProcessor linesProcessor() {
        return new LinesProcessor();
    }

    @Bean
    public LinesWriter linesWriter() {
        return new LinesWriter();
    }

    @Bean
    public Job readCSVFilesJob() {
        return jobBuilderFactory
                .get( "readCSVFilesJob" )
                .start( readDat() )
                .next( processDat() )
                .next( writeDat() )
                .build();
    }

    @Bean
    public Step readDat() {
        return stepBuilderFactory
                .get( "readDat" )
                .tasklet( linesReader() )
                .build();
    }

    @Bean
    public Step processDat() {
        return stepBuilderFactory
                .get( "processDat" )
                .tasklet( linesProcessor() )
                .build();
    }

    public Step writeDat() {
        return stepBuilderFactory
                .get( "writeDat" )
                .tasklet( linesWriter() )
                .build();
    }

}
