package com.dbccompany.SalesManagement.DTO;

import java.io.Serializable;

public class Item implements Serializable {
    private String id;
    private int quantity;
    private float price;

    public Item ( String id, int quantity, float price ) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
