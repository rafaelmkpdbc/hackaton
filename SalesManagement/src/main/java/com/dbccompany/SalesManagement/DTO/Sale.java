package com.dbccompany.SalesManagement.DTO;

import java.io.Serializable;
import java.util.ArrayList;

public class Sale implements Serializable {
    private String id;
    private String seller;
    private ArrayList<Item> items;

    public Sale( String id, String seller, ArrayList<Item> items ) {
        this.id = id;
        this.seller = seller;
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void addOneItem( Item item ) {
        this.items.add( item );
    }
}
