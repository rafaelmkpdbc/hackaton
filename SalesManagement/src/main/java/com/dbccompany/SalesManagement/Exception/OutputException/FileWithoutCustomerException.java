package com.dbccompany.SalesManagement.Exception.OutputException;

public class FileWithoutCustomerException extends OutputException {
    public FileWithoutCustomerException() {
        super("O arquivo de saída não possui a quantidade de clientes.");
    }
}
