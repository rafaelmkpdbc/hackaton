package com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class CNPJInvalidException extends ModelsException {
    public CNPJInvalidException() {
        super("O CNPJ de cliente é inválido.");
    }
}
