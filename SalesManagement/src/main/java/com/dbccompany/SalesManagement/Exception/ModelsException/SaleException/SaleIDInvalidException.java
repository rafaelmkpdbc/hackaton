package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class SaleIDInvalidException extends ModelsException {
    public SaleIDInvalidException() {
        super("O ID da venda é inválido.");
    }
}
