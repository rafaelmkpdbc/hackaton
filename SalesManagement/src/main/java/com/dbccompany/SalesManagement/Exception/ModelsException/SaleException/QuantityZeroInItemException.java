package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class QuantityZeroInItemException extends ModelsException {
    public QuantityZeroInItemException() {
        super("A quantidade de itens é 0.");
    }
}
