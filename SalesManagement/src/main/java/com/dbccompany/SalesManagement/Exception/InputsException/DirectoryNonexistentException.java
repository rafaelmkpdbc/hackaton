package com.dbccompany.SalesManagement.Exception.InputsException;

public class DirectoryNonexistentException extends InputException {
    public DirectoryNonexistentException() {
        super("O diretório do arquivo não existe.");
    }
}
