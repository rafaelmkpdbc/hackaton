package com.dbccompany.SalesManagement.Exception.OutputException;

public class FileWithoutSellerException extends OutputException{
    public FileWithoutSellerException() {
        super("O arquivo de saída não possui a quantidade de vendedores.");
    }
}
