package com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class MissingDataCustomerException extends ModelsException {
    public MissingDataCustomerException() {
        super("Falta informações para cliente.");
    }
}
