package com.dbccompany.SalesManagement.Exception.InputsException;

public class FileWithoutSellerException extends InputException{
    public FileWithoutSellerException() {
        super("O arquivo não possui vendedores.");
    }
}
