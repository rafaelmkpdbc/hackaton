package com.dbccompany.SalesManagement.Exception.ModelsException;

public class TooManyDataException extends ModelsException{
    public TooManyDataException() {
        super("Muitos atributos fornecidos.");
    }
}
