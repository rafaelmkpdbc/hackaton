package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class MissingDataSaleExpcetion extends ModelsException {
    public MissingDataSaleExpcetion() {
        super("Falta informações para uma venda.");
    }
}
