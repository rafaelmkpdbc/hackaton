package com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class NameInvalidException extends ModelsException {
    public NameInvalidException() {
        super("O nome do cliente é inválido.");
    }
}
