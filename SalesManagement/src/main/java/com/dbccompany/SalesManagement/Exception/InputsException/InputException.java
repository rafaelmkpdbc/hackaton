package com.dbccompany.SalesManagement.Exception.InputsException;

public class InputException extends Exception {
    public InputException(String message){
        super(message);
    }
}
