package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class SellerNonexistentException extends ModelsException {
    public SellerNonexistentException() {
        super("O vendedor é inexistente.");
    }
}
