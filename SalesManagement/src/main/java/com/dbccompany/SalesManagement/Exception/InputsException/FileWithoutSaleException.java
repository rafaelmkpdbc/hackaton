package com.dbccompany.SalesManagement.Exception.InputsException;

public class FileWithoutSaleException extends InputException{
    public FileWithoutSaleException() {
        super("O arquivo não possui vendas.");
    }
}
