package com.dbccompany.SalesManagement.Exception.ModelsException;

public class InvalidSeparatorCharException extends ModelsException{
    public InvalidSeparatorCharException() {
        super("Caracter de separador inválido.");
    }
}
