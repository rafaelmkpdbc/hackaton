package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class PriceNegativeInItemException extends ModelsException {
    public PriceNegativeInItemException() {
        super("O preço do item na venda é negativo.");
    }
}
