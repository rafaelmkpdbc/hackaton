package com.dbccompany.SalesManagement.Exception.ModelsException.SellerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class MissingDataSellerExpcetion extends ModelsException {
    public MissingDataSellerExpcetion() {
        super("Falta informações para vendedor.");
    }
}
