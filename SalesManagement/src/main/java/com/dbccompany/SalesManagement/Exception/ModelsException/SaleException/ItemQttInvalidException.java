package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class ItemQttInvalidException extends ModelsException {
    public ItemQttInvalidException() {
        super("A quantidade de itens é inválida.");
    }
}
