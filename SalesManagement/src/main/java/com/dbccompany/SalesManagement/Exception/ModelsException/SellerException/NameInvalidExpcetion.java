package com.dbccompany.SalesManagement.Exception.ModelsException.SellerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class NameInvalidExpcetion extends ModelsException {
    public NameInvalidExpcetion() {
        super("O nome do vendedor é inválido.");
    }
}
