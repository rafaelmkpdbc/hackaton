package com.dbccompany.SalesManagement.Exception.OutputException;

public class BlankFileException extends OutputException {
    public BlankFileException() {
        super("O arquivo de saída está em branco.");
    }
}
