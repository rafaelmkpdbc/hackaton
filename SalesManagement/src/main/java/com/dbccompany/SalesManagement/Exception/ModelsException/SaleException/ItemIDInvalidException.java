package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class ItemIDInvalidException extends ModelsException {
    public ItemIDInvalidException() {
        super("O ID do item é inválido.");
    }
}
