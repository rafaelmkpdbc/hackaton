package com.dbccompany.SalesManagement.Exception.ModelsException.CustomerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class CustomerIDInvalidException extends ModelsException {
    public CustomerIDInvalidException() {
        super("O identificador de cliente é inválido. (002)");
    }
}
