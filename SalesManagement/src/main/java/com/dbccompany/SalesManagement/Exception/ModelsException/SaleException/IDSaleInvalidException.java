package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class IDSaleInvalidException extends ModelsException {
    public IDSaleInvalidException() {
        super("O identificador de venda é inválido. (003)");
    }
}
