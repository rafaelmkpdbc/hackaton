package com.dbccompany.SalesManagement.Exception.OutputException;

public class FileWithoutWorstSellerException extends OutputException{
    public FileWithoutWorstSellerException() {
        super("O arquivo de saída não possui a informação do pior vendedor.");
    }
}
