package com.dbccompany.SalesManagement.Exception.ModelsException.SellerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class CPFInvalidException extends ModelsException {
    public CPFInvalidException() {
        super("O CPF do vendedor é inválido.");
    }
}
