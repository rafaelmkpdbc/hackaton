package com.dbccompany.SalesManagement.Exception.InputsException;

public class ExtensionInvalidException extends InputException {
    public ExtensionInvalidException(){
        super("A extensão do arquivo não é compatível. (.dat)");
    }
}
