package com.dbccompany.SalesManagement.Exception.OutputException;

public class OutputException extends Exception {
    public OutputException(String message){
        super(message);
    }
}
