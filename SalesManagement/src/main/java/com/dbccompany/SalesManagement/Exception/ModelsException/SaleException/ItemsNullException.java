package com.dbccompany.SalesManagement.Exception.ModelsException.SaleException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class ItemsNullException extends ModelsException {
    public ItemsNullException() {
        super("Os itens são inexistentes.");
    }
}
