package com.dbccompany.SalesManagement.Exception.InputsException;

public class FileWithoutCustomerException extends InputException{
    public FileWithoutCustomerException() {
        super("O arquivo não possui clientes.");
    }
}
