package com.dbccompany.SalesManagement.Exception.OutputException;

public class DirectoryNonexistentException extends OutputException{
    public DirectoryNonexistentException() {
        super("O diretório de saída não existe.");
    }
}
