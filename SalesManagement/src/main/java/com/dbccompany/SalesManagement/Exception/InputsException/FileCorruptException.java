package com.dbccompany.SalesManagement.Exception.InputsException;

public class FileCorruptException extends InputException {
    public FileCorruptException() {
        super("O arquivo está corrompido.");
    }
}
