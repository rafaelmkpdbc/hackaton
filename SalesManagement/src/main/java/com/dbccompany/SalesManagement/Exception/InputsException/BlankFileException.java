package com.dbccompany.SalesManagement.Exception.InputsException;

public class BlankFileException extends InputException{
    public BlankFileException() {
        super("O arquivo de input está em branco.");
    }
}
