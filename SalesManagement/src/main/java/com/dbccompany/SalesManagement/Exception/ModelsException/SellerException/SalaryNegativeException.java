package com.dbccompany.SalesManagement.Exception.ModelsException.SellerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class SalaryNegativeException extends ModelsException {
    public SalaryNegativeException() {
        super("O salário do vendedor é negativo.");
    }
}
