package com.dbccompany.SalesManagement.Exception.OutputException;

public class FileWithoutIDSaleException extends OutputException {
    public FileWithoutIDSaleException() {
        super("O arquivo de saída não possui o ID da venda mais cara.");
    }
}
