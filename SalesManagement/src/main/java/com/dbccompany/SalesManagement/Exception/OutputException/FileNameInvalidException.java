package com.dbccompany.SalesManagement.Exception.OutputException;

public class FileNameInvalidException extends OutputException{
    public FileNameInvalidException() {
        super("O nome do arquivo está fora dos padrões.");
    }
}
