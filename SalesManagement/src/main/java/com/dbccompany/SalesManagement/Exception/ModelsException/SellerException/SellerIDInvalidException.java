package com.dbccompany.SalesManagement.Exception.ModelsException.SellerException;

import com.dbccompany.SalesManagement.Exception.ModelsException.ModelsException;

public class SellerIDInvalidException extends ModelsException {
    public SellerIDInvalidException() {
        super("O identificador de vendedor é inválido. (001)");
    }
}
