package com.dbccompany.SalesManagement.Exception.ModelsException;

public class ModelsException extends Exception {
    public ModelsException(String message){
        super(message);
    }
}
