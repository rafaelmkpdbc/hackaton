package com.dbccompany.SalesManagement.Exception.InputsException;

public class IDTypeInvalidException extends InputException{
    public IDTypeInvalidException() {
        super("ID do tipo (cliente, vendedor, venda) não identificado.");
    }
}
