package com.dbccompany.SalesManagement;

import com.dbccompany.SalesManagement.Util.DatUtils;
import com.dbccompany.SalesManagement.Util.Log;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.batch.operations.JobExecutionAlreadyCompleteException;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;

@Profile("!test")
@Component
public class WatchFilesStartup {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @EventListener(ApplicationReadyEvent.class)
    public void watchFiles() throws IOException, InterruptedException {
        WatchService watchService = FileSystems.getDefault().newWatchService();
        DatUtils.checkDirectoryIn();
        DatUtils.checkDirectoryOn();
        DatUtils.DIRECTORY_PATH_IN.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        WatchKey key;

        while ((key = watchService.take()) != null) {
            List<WatchEvent> events = key.pollEvents()
                    .stream()
                    .filter(event -> DatUtils.filterExtension(event.context().toString()))
                    .collect(Collectors.toList());

            for (WatchEvent<?> event : events) {
                JobParameters parameters = new JobParametersBuilder()
                        .addString("filename", String.valueOf(event.context()))
                        .toJobParameters();
                try {
                    jobLauncher.run(job, parameters);
                } catch (JobExecutionAlreadyRunningException
                        | JobRestartException
                        | JobExecutionAlreadyCompleteException
                        | JobParametersInvalidException
                        | JobInstanceAlreadyCompleteException e) {
                    Log.registraErro(e.getMessage());
                    continue;
                }
            }
            key.reset();
        }
    }
}
