# AUTOMATED SALES REPORT

Automated Sales Report é uma aplicação voltada ao processamento de dados de vendas, através da leitura e escrita em arquivos `.dat`.

![Badge](https://img.shields.io/badge/Java-8-informational?style=plastic&logo=java)
![Badge](https://img.shields.io/badge/JUnit-5-informational?style=plastic&logo=junit5)
![Badge](https://img.shields.io/badge/Spring-2.5.4-informational?style=plastic&logo=spring)
![Badge](https://img.shields.io/badge/Batch-4.3.3-informational?style=plastic&logo=spring)
![Badge](https://img.shields.io/badge/Docker-20.10.2-informational?style=plastic&logo=docker)

___

### Status do Projeto

🚀 CONCLUÍDO 🚀

## Tabela de Conteúdo

- 1. Funcionalidades
- 2. Pré-Requisitos
- 3. Instalação
- 4. Tecnologias utilizadas 
___

## 1. Funcionalidades :dart:

- Leitura automatizada de dados de vendas em arquivos
- Processamento dos dados
- Geração de relatórios a partir dos dados

  A aplicação processa informações relacionadas a vendas, recebendo os seguintes dados: `vendedores`, `clientes` e `vendas de fato`.   
  <br>
  Com isso, serão gerados os seguintes dados: `pior vendedor`, `quantidade de vendedores`, `ID da venda mais cara` e `quantidade de clientes`.


## 2. Pré-Requisitos :gear:

Para utilização da aplicação, é necessário possuir em sua máquina as seguintes tecnologias:
- [Java JRE](https://www.oracle.com/java/technologies/javase-downloads.html) - versão `8.0` ou mais recente.
- [Docker](https://docker.com) - versão `20.0` ou mais recente.

## 3. Instalação :gear:

Criar uma pasta para baixar o projeto
```
mkdir automated-sales-report
```
Acessar a pasta recém-criada e clonar o repositório git.
```
cd automated-sales-report
git clone https://rafaelmkpdbc@bitbucket.org/rafaelmkpdbc/hackaton.git
```
Acessar a pasta da aplicação
```
cd SalesManagement
```
Criar uma imagem `docker` da aplicação
```
docker build -t automated-sales-report .
```
Criar um contêiner com `docker` a partir da imagem criada. O comando já executa o próprio contêiner. 
```
docker run -v ~/data:/root/data:rw automated-sales-report
```


### 3.1. Iniciando a aplicação    💻

A aplicação deve ser iniciada através do seguinte comando:
```
(COMANDO DOCKER)
```   
O comando executa o contêiner construído através da imagem fornecida, informando uma pasta física do `host` que será espelhada em uma pasta virtual do contêiner.   

### 3.2. Utilizando a aplicação  💻

Iniciada a aplicação, ela ficará aguardando a criação de arquivos em uma pasta específica, no seguinte caminho:

``` %HOMEPATH%/data/in ```

Onde `%HOMEPATH%` é o diretório de usuário no sistema operacional 
>>Em sistemas Windows, `C:\\Users\{seu-usuario}`   
>>Em sistemas Mac, `/home/{seu-usuario}`   
>>Em sistemas Linux, `HD/Users/home/{seu-usuario}`   

:warning: Importante destacar que o sistema não reconhecerá a alteração ou deleção de arquivos dentro da pasta, apenas a criação de novos arquivos.   
   
Ao inserir os arquivos, o sistema automaticamente fará o processamento dos dados e criará um arquivo de saída na seguinte pasta:
``` %HOMEPATH%/data/out ```

### 3.3. Formato de arquivos de entrada 📄

Dados obrigatórios para cada entidade   

| Vendedor | Cliente | Venda |
|----------|---------|-------|
| CPF  | CNPJ | ID |
| Nome | Nome | Lista de itens |
| Salário | Área de negócio | Vendedor


Os arquivos de entrada devem ter a extensão `.dat`.   

São requisitos do arquivo para o correto processamento das informações: 
- Seguir uma sequência de dados: `vendedor`, `cliente`, `venda`.
- Apenas uma entidade por linha do arquivo.
- Utilizar `ç` como separador entre dados de cada entidade.
- Utilizar `-` como separar entre dados de cada item de venda.

<br>
Exemplo de arquivo de entrada:

```
001ç1234567891234çPedroç50000
001ç3245678865434çPauloç40000.99
002ç2345675434544345çJose da SilvaçRural
002ç2345675433444345çEduardo PereiraçRural
003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro
003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo
```

### 3.4. Formato de arquivos de saída 📄

O arquivo de saída terá a extensão `.done.dat`.   
O arquivo terá o mesmo nome do arquivo de entrada.


  Exemplo de arquivo de saída:    

```
Quantidade de Clientes: 2
Quantidade de Vendedores: 2
ID Venda mais alta: 10
Pior Vendedor: Paulo
```

## 4. Tecnologias Utilizadas 🛠️ 
- [Java 8]()
- JUnit5
- Spring Boot
- Spring Batch
- Docker
___

### Colaboradores
[![Badge](https://img.shields.io/badge/victor--haberkamp%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/v%C3%ADctor-hugo-haberkamp-257b551aa/)
[![Badge](https://img.shields.io/badge/rafael--panzenhagen%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/rafael-panzenhagen/)
[![Badge](https://img.shields.io/badge/filipe--ferreira%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/filipeferreirabr/) 
[![Badge](https://img.shields.io/badge/matheus--canabarro%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white")](https://www.linkedin.com/in/matheus-canabarro-b8a5b11b1/)


### Licença
MIT