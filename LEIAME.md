# AUTOMATED SALES REPORT

Automated Sales Report é uma aplicação voltada ao processamento de dados de vendas, através da leitura e escrita em arquivos `.dat`.

badges   
![Badge](https://img.shields.io/badge/Java-8-informational?style=plastic&logo=java)
![Badge](https://img.shields.io/badge/Spring-8-informational?style=plastic&logo=spring)
java junit spring spring-batch nio2 docker 
___

### Status do Projeto

🚀 CONCLUÍDO 🚀

## Tabela de Conteúdo

- ipsum lorem
- ipsum lorem
- ipsum lorem
- ipsum lorem   
___

## . Funcionalidades  :dart:

- Leitura automatizada de dados de vendas em arquivos
- Processamento dos dados
- Geração de relatórios a partir dos dados
- 

## . Pré-Requisitos :memo:

## . Instalação :gear:

ipsum lorem

## . Como Utilizar

ipsum lorem

### .. Formato de arquivos de entrada

ipsum lorem

### .. Formato de arquivos de saída

ipsum lorem

## . Tecnologias Utilizadas :hammer_and_wrench:
ipsum lorem
___

### Colaboradores
badges (linkedin)

### Licença
MIT